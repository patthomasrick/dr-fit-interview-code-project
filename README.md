# Dr. Fit Interview Code Project

Submission for Dr. Fit's Interview process.

Test this project live at [https://interview.patrickwthomas.net/](https://interview.patrickwthomas.net/). The API is hosted separately at [https://api.patrickwthomas.net/](https://api.patrickwthomas.net/). _Note that HTTPS is required, otherwise the API might behave unexpectedly._

**Software requirements:**

- MySQL 8 (tested and also works on 5.6.43)
- PHP 7.4 (test and also works on 7.3.28)

## Setup

### Docker Compose

First, Composer needs to install its dependencies:

```sh
composer install
```

To quickly get the development server up and running, you'll need [Docker](https://www.docker.com/get-started) and [Docker Compose](https://docs.docker.com/compose/install/). Once those are installed, a local development server can be started via the below commands:

```sh
git clone "git@gitlab.com:patthomasrick/dr-fit-interview-code-project.git"
cd "dr-fit-interview-code-project"
docker-compose up
# Server should be started in about a minute...
```

The MySQL server might take a minute or two to fully initialize the first time. After about a minute, the API should be live at [http://localhost:8080/](http://localhost:8080/). Likewise, PHPMyAdmin is live at [http://localhost:8080/](http://localhost:8080/). Note that [interview.js](src/static/interview.js) by default points to the dev server at [https://api.patrickwthomas.net/](https://api.patrickwthomas.net/), not the locally hosted API. This can be configured easily via configuring [line 1 of interview.js](https://gitlab.com/patthomasrick/dr-fit-interview-code-project/-/blob/main/src/static/interview.js#L1) to point to `http://localhost:8080`.

### Manual Setup

The server can also be manually installed with a bit of configuring. In general:

1. _Composer:_
   1. `composer install`
2. _Setting database connection information:_
   1. Method 1: Set environment variables (in either somewhere like `.bashrc` or in `.htaccess`)
      1. `DB_HOST` - Host of the database server, like `localhost` or `patrickwthomas.net`.
      2. `DB_PORT` - Port of the database. For MySQL, this should be `3306`.
      3. `DB_NAME` - Name of the database, like `interview`.
      4. `DB_USER` - Database username. The database user needs `SELECT`, `UPDATE`, `INSERT`, and `DELETE` permissions.
      5. `DB_PASSWORD` - Password for the aforementioned database user.
      6. To configure mailing (required for unlock API to work), `MAIL_` constants from [Secret.php](https://gitlab.com/patthomasrick/dr-fit-interview-code-project/-/blob/main/include/Secret.php) must also be configured in a similar manner as above.
   2. Method 2: Manually configure [Secret.php](https://gitlab.com/patthomasrick/dr-fit-interview-code-project/-/blob/main/include/Secret.php).
      1. See Method 1 for a description of the parameters. Either the default value can be changed (like `"mysql"`) or the entire expression can be replaced with some fixed value.
3. _Point the test API to the correct URL:_
   1. [interview.js](src/static/interview.js) by default points to the dev server at [https://api.patrickwthomas.net/](https://api.patrickwthomas.net/), not the locally hosted API. This can be configured easily via configuring [line 1 of interview.js](https://gitlab.com/patthomasrick/dr-fit-interview-code-project/-/blob/main/src/static/interview.js#L1) to point to `http://localhost:8080`.
4. _Installation:_
   1. `src` needs to be the web root of your project, like `/var/www/html`.
   2. `include` needs to be _outside_ of `src` since the APIs in `src` relatively access the files in `include`.
   3. `index.html` and `static/` in `src` can either be placed on the same server or some different server as long as all of the previous variables were configured.

## Testing

Running `./vendor/bin/phpunit tests` will test all database-agnostic interfaces and does not require a development server to be running. Running `./vendor/bin/phpunit online-test`, however, _does_ require a development server to be running or configured. Should the server be inaccessible, most, if not all, tests will be skipped.

## API Documentation

All endpoints accept data via JSON payloads in POST requests. The JSON data can either be base 64 encoded or sent as-is without any encoding. All responses are also in JSON payloads, although unencoded.

Responses can return in any of the following statuses: `success`, `failed`, or `error`. Success is self-explanatory and indicates that the request was processed, interpreted, and handled successfully and with no errors. Failed means that some part of the request could not be completed, like if an invalid authentication hash is used. Finally, error is returned in the event that some part of handling the request failed such that it could not continue to be completed.

### [`api/user/authenticate.php`](https://api.patrickwthomas.net/api/user/authenticate.php) (POST)

Given some authentication hash (generated after logging in), test whether or not the authentication hash is valid (is not expired and is the most recent hash for a user).

**Parameters**

- `authentication_hash` (required) - Authentication key/hash given after logging in.

**Example input**

```json
{
  "authentication_hash": "e847de9102032cd1ed4ddb6580e175c937a4fee13d47b7400885a1d897a52564"
}
```

**Example response**

```json
{
  "timestamp": "2021-07-07 11:28:38",
  "status": "success"
}
```

### [`api/user/create.php`](https://api.patrickwthomas.net/api/user/create.php) (POST)

Create a new user account. Calling `login` is still needed in order to obtain an API hash. A username, password, and email are required to create a user account. First and last name are optional.

**Parameters**

- `username` (required) - Username to create a user account under. Must be at least three characters long and be composed of some combination of valid alphanumeric characters or any of the following symbols: `.!#$%*+-=?\^_\{|}~`.
- `email` (required) - Email address associated with the account.
- `password` (required) - Used to authenticate the user. Must be at least eight characters long.
- `first_name` (optional) - First name of the user.
- `last_name` (optional) - Last name of the user.

**Example input**

```json
{
  "username": "patrick",
  "email": "patrick@patrickwthomas.net",
  "password": "password",
  "first_name": "Patrick",
  "last_name": "Thomas"
}
```

**Example response**

```json
{
  "timestamp": "2021-07-07 11:28:38",
  "status": "success"
}
```

### [`api/user/delete.php`](https://api.patrickwthomas.net/api/user/delete.php) (POST)

This simply deletes an account. A username/email and password pair is needed for this operation. All associated authentication hashes are deleted and therefore invalidated.

Similar to login, only one of either a username or email is needed to authenticate the account. Both can be provided, but they both must match the same account.

**Parameters**

- `username` (required/optional) - Needed to identify the account. Only needed if no email is provided.
- `email` (required/optional) - Needed to identify the account. Only needed if no username is provided.
- `password` (required) - Used to authenticate the user.

**Example input**

```json
{
  "username": "patrick",
  "password": "password"
}
```

**Example response**

```json
{
  "timestamp": "2021-07-07 11:28:38",
  "status": "success"
}
```

### [`api/user/login.php`](https://api.patrickwthomas.net/api/user/login.php) (POST)

Login in to an account and get a new authentication hash to use for further communications with the API. An account must be active in order for this operation to succeed. Getting a new API key automatically invalidates all other keys.

Only one of either a username or email is needed to authenticate the account. Both can be provided, but they both must match the same account.

Failure to log in five times in a row will lock the account and send a unlock URL to the registered email.

**Parameters**

- `username` (required/optional) - Needed to identify the account. Only needed if no email is provided.
- `email` (required/optional) - Needed to identify the account. Only needed if no username is provided.
- `password` (required) - Used to authenticate the user.

**Example input**

```json
{
  "username": "patrick",
  "password": "password"
}
```

**Example response**

```json
{
  "authentication_hash": "e847de9102032cd1ed4ddb6580e175c937a4fee13d47b7400885a1d897a52564",
  "timestamp": "2021-07-07 11:28:38",
  "status": "success"
}
```

### [`api/user/unlock.php`](https://api.patrickwthomas.net/api/user/unlock.php) (GET)

With an unlock code, set a user from inactive to active. Unlock codes expire within one day of being sent.

**Parameters**

- `username` (required/optional) - Needed to identify the account. Only needed if no email is provided.
- `email` (required/optional) - Needed to identify the account. Only needed if no username is provided.
- `password` (required) - Used to authenticate the user.

**Example input**

```
https://api.patrickwthomas.net/api/user/unlock.php?unlock_code=e847de9102032cd1ed4ddb6580e175c937a4fee13d47b7400885a1d897a52564
```

**Example response**

```json
{
  "timestamp": "2021-07-08 11:55:19",
  "status": "success"
}
```

## Requirements

- [x] create a SSO API endpoint for application authentication
  - [x] takes username or email and a password from any platform (web, mobile, desktop app, etc)
  - [x] API handles authentication (DB lookup, password validation, verification)
- [x] JSON response acknowledging if authentication request succeeded or failed
- [x] JSON response should include a hash for use for future requests for authentication

### Minimum Requirements

- [x] DB, API, login functionality should be OO and show proper inheritance usage, DI, etc.
- [x] MySQL DB
  - [x] two tables
    - [x] users - id, username, password, first name, last name, email, active user (boolean to track deleted users, `true` means active)
    - [x] logins - id, login date time, successful (track unsuccessful login attempts), user ID, authentication hash, IP address, expires
- [x] web-based login form that utilizes the API to authenticate a user and proves if an attempt was successful/unsuccessful

### Extra Credit

- [x] account lock out on 5 failed attempts and send an email to the user to unlock their account or reset their password
- [ ] log suspicious log in attempts to the database in another table (based on IP address and time between login attempts)
- [x] have correct primary, foreign, and unique keys set up on tables
- [x] have API on a separate domain and the login form send a request to the API's domain/endpoint
- [x] encrypt/encode API request data (base64 encode the request before TX to API)
- [x] provide unit test for the code
