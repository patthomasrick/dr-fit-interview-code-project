-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 08, 2021 at 10:15 AM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pwt5ca_interview`
--

-- --------------------------------------------------------

--
-- Table structure for table `logins`
--

CREATE TABLE `logins` (
  `id` int(16) UNSIGNED NOT NULL,
  `login_date_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `successful` tinyint(1) NOT NULL,
  `user_id` int(16) UNSIGNED NOT NULL,
  `authentication_hash` varchar(64) DEFAULT NULL,
  `ip_address` varchar(15) NOT NULL,
  `expires` datetime DEFAULT NULL
);

--
-- Triggers `logins`
--
DELIMITER $$
CREATE TRIGGER `before_logins_insert` BEFORE INSERT ON `logins` FOR EACH ROW IF new.successful THEN
    SET new.expires = COALESCE(new.expires, DATE_ADD(NOW(), INTERVAL 10 DAY));
END IF
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `suspicious_attempts`
--

CREATE TABLE `suspicious_attempts` (
  `id` int(16) UNSIGNED NOT NULL,
  `user_id` int(16) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `attempt_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
);

-- --------------------------------------------------------

--
-- Table structure for table `unlock`
--

CREATE TABLE `unlock` (
  `id` int(16) UNSIGNED NOT NULL,
  `user_id` int(16) UNSIGNED NOT NULL,
  `unlock_code` varchar(64) NOT NULL,
  `creation_date_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `expiration_date_time` datetime DEFAULT NULL
);

--
-- Triggers `unlock`
--
DELIMITER $$
CREATE TRIGGER `before_unlock_insert` BEFORE INSERT ON `unlock` FOR EACH ROW SET new.expiration_date_time = COALESCE(new.expiration_date_time, DATE_ADD(NOW(), INTERVAL 1 DAY))
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(16) UNSIGNED NOT NULL,
  `username` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `first_name` varchar(64) DEFAULT NULL,
  `last_name` varchar(64) DEFAULT NULL,
  `active_user` tinyint(1) NOT NULL DEFAULT '1'
);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `logins`
--
ALTER TABLE `logins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `suspicious_attempts`
--
ALTER TABLE `suspicious_attempts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `unlock`
--
ALTER TABLE `unlock`
  ADD PRIMARY KEY (`id`),
  ADD KEY `unlock_ibfk_1` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `logins`
--
ALTER TABLE `logins`
  MODIFY `id` int(16) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `suspicious_attempts`
--
ALTER TABLE `suspicious_attempts`
  MODIFY `id` int(16) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `unlock`
--
ALTER TABLE `unlock`
  MODIFY `id` int(16) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(16) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `logins`
--
ALTER TABLE `logins`
  ADD CONSTRAINT `logins_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `suspicious_attempts`
--
ALTER TABLE `suspicious_attempts`
  ADD CONSTRAINT `suspicious_attempts_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `unlock`
--
ALTER TABLE `unlock`
  ADD CONSTRAINT `unlock_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
