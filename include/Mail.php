<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require_once __DIR__ . "/Config.php";
require_once __DIR__ . "/Secret.php";
require_once __DIR__ . "/User.php";

function send_email(string $to, string $subject, string $body) {
  if (!MAIL_ENABLED) {
    return false;
  }
  $mail = new PHPMailer(true);
  try {
    // Server settings
    // $mail->SMTPDebug = SMTP::DEBUG_SERVER;
    $mail->isSMTP();
    $mail->Host = MAIL_HOST;
    $mail->SMTPAuth = MAIL_SMTP_AUTH;
    $mail->Username = MAIL_USERNAME;
    $mail->Password = MAIL_PASSWORD;
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
    $mail->Port = MAIL_PORT;

    // Recipients
    $mail->setFrom(MAIL_FROM_EMAIL, MAIL_FROM_NAME);
    $mail->addReplyTo(MAIL_FROM_EMAIL, MAIL_FROM_NAME);
    $mail->addAddress($to);

    // Content
    $mail->isHTML(true);
    $mail->Subject = "$subject";
    $mail->Body = wordwrap("$body", 70, "\r\n");
    $mail->AltBody = wordwrap("$body", 70, "\r\n");

    $mail->send();
    return true;
  } catch (Exception $e) {
    return false;
  }
}
