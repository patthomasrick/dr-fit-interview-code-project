<?php

require_once __DIR__ . "/Config.php";

/**
 * Endpoint provides interfaces to safely execute API code and always provide some default JSON
 * response when an error is encountered.
 */
abstract class Endpoint {
  private string $status = "success";
  private bool $encoded = false;
  protected array $output_data = [];

  /**
   * Construct a new endpoint.
   *
   * @param  bool $encoded Whether or not to respond with a base 64 encoded response.
   * @return void
   */
  public function __construct(bool $encoded = false) {
    $this->encoded = $encoded;
  }

  /**
   * Safely run the API's code. Any unhandled exceptions are caught and handled.
   *
   * @return void
   */
  public function run() {
    try {
      $this->status = $this->_run() ? "success" : "failed";
    } catch (Exception $e) {
      $this->status = "error";
      $this->update_response_value("error", $e->getMessage());
    } finally {
      $currentDate = new DateTime();
      $this->update_response_value("timestamp", $currentDate->format("Y-m-d H:i:s"));
      $this->exit();
    }
  }

  /**
   * User code, designed to encapsulate the API's main functionality.
   *
   * @return bool True if the code succeeded, false otherwise. This is reflected in the API's final
   * response back to the client.
   */
  abstract protected function _run(): bool;

  /**
   * Update some value intended to be in the API's response.
   *
   * @param  string $key Key for the response.
   * @param  mixed $value Value, can be anything that can be successfully JSON-encoded.
   * @return void
   */
  protected function update_response_value(string $key, $value) {
    $this->output_data[$key] = $value;
  }

  /**
   * Immediately stop executing the API code and respond with an error.
   *
   * @param  string $message Error message to display to the client.
   * @return void
   */
  protected function error(string $message): void {
    throw new Exception($message);
  }

  /**
   * Stop execution and echo the response back to the client.
   *
   * @return void
   */
  private function exit(): void {
    $this->output_data["status"] = $this->status;
    $output = json_encode($this->output_data, JSON_PRETTY_PRINT);
    if ($this->encoded) {
      $output = base64_encode($output);
    }
    echo "$output";
  }
}

/**
 * SimpleEndpoint always returns some constant value to the client.
 */
final class SimpleEndpoint extends Endpoint {
  public function __construct(array $output_data, bool $encoded = false) {
    parent::__construct($encoded);
    $this->output_data = array_merge($this->output_data, $output_data);
  }

  protected function _run(): bool {
    return true;
  }
}

/**
 * ErrorEndpoint always returns some error to the client.
 */
final class ErrorEndpoint extends Endpoint {
  private Throwable $error;

  public function __construct(Throwable $th, bool $encoded = false) {
    parent::__construct($encoded);
    $this->error = $th;
  }

  protected function _run(): bool {
    throw $this->error;
  }
}
