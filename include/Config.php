<?php

declare(strict_types=1);

require_once __DIR__ . "/../vendor/autoload.php";

try {
  header("Access-Control-Allow-Origin: *");
  header("Access-Control-Allow-Methods: GET, POST");
  header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
  header("Content-Type: application/json; charset=UTF-8");
} catch (\Throwable $th) {
  // Couldn't disable CORS.
}

date_default_timezone_set("America/New_York");

const DEFAULT_EXPIRATION_DELAY = 3600;
const MINIMUM_PASSWORD_LENGTH = 8;
const ATTEMPTS_UNTIL_LOCKOUT = 5;
const HASH_ALGO = "sha256";

const USERNAME_REGEX = '^[ \w\.!#$%*+\-/=?\^_`{|}~]{3,}^';
const EMAIL_REGEX = '^[\w\.!#$%*+\-/=?\^_`{|}~]+@([\w]+\.)*([\w]+)(\.\w{2,})+^';
