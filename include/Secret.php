<?php

require_once __DIR__ . "/Config.php";
require_once __DIR__ . "/Utilities.php";

// Needs to be changed per-configuration. Settings here should agree with the Docker dev server.
// Alternatively, setting environment variables also will suffice.

define("DB_HOST", get_env_or_default("DB_HOST", "mysql"));
define("DB_PORT", get_env_or_default("DB_PORT", "3306"));
define("DB_NAME", get_env_or_default("DB_NAME", "interview"));
define("DB_USER", get_env_or_default("DB_USER", "root"));
define("DB_PASSWORD", get_env_or_default("DB_PASSWORD", "secret"));
define("DB_DSN", "mysql:host=" . DB_HOST . ";port=" . DB_PORT . ";dbname=" . DB_NAME);

// PHPMailer settings
define("MAIL_ENABLED", boolval(get_env_or_default("MAIL_ENABLED", "")));
define("MAIL_HOST", get_env_or_default("MAIL_HOST", ""));
define("MAIL_SMTP_AUTH", boolval(get_env_or_default("MAIL_SMTP_AUTH", "")));
define("MAIL_USERNAME", get_env_or_default("MAIL_USERNAME", ""));
define("MAIL_PASSWORD", get_env_or_default("MAIL_PASSWORD", ""));
define("MAIL_PORT", get_env_or_default("MAIL_PORT", ""));
define("MAIL_FROM_EMAIL", get_env_or_default("MAIL_FROM_EMAIL", ""));
define("MAIL_FROM_NAME", get_env_or_default("MAIL_FROM_NAME", ""));
