<?php

require_once __DIR__ . "/Config.php";
require_once __DIR__ . "/Database.php";

/**
 * Determine if a string is a valid email address or not.
 *
 * @param  string $value String to check.
 * @return bool True if the value is an email, false otherwise.
 */
function is_valid_email(string $value): bool {
  return is_string($value) && preg_match(EMAIL_REGEX, $value) === 1;
}

/**
 * Determine if a string is a valid username or not.
 *
 * @param  string $value String to check.
 * @return bool True if the value is an username, false otherwise.
 */
function is_valid_username(string $value): bool {
  return is_string($value) && preg_match(USERNAME_REGEX, $value) === 1;
}

/**
 * Validate passwords.
 *
 *  Requirements:
 *  - password length is at least 8 characters long.
 *
 * @param  string $value Value to validate.
 * @return bool
 */
function is_valid_password(string $value): bool {
  return is_string($value) && strlen($value) > 8;
}

/**
 * Identity is a superclass of authenticate-able types that provides access to usernames, emails,
 * or authentication hashes as well as validation for such values.
 */
class Identity {
  private string $username;
  private string $email;
  private string $authentication_hash;
  private string $password;
  private array $valid_identities;

  /**
   * Construct a new identity, which can be used to log into the server. One of either a username,
   * email, or authentication key is required.
   *
   * @param  string $username
   * @param  string $email
   * @param  string $authentication_hash
   * @return void
   */
  protected function __construct(
    string $username = "",
    string $email = "",
    string $password = "",
    string $authentication_hash = ""
  ) {
    // Determine which identities are valid for readability later on.
    $valid_username = is_string($username) && !empty($username) && is_valid_username($username);
    $valid_email = is_string($email) && !empty($email) && is_valid_email($email);
    $valid_password =
      is_string($password) && !empty($password) && strlen($password) >= MINIMUM_PASSWORD_LENGTH;
    $valid_authentication_hash = is_string($authentication_hash) && !empty($authentication_hash);

    // Need at least a username/email and password pair or a valid authentication hash.
    if (!$valid_username && !$valid_email && !$valid_authentication_hash) {
      throw new Exception(
        "Invalid identity provided; no credentials provided. Need at least a username/email and password pair or a valid authentication hash."
      );
    } elseif (
      (($valid_username || $valid_email) && $valid_password) ||
      $valid_authentication_hash
    ) {
      // Valid combination.
    } elseif (($valid_username || $valid_email) && !$valid_password) {
      throw new Exception(
        "Invalid identity provided; username and email are valid, but password is invalid."
      );
    }

    $this->username = $username;
    $this->email = $email;
    $this->password = $password;
    $this->authentication_hash = $authentication_hash;

    $this->valid_identities = [
      "username" => $valid_username,
      "email" => $valid_email,
      "password" => $valid_password,
      "authentication_hash" => $valid_authentication_hash,
    ];
  }

  /**
   * Make a new identity with a username/email and password. At least one of either a username or
   * email is required.
   *
   * @return Identity
   */
  public static function make_with_password(
    string $username = "",
    string $email = "",
    string $password = ""
  ): Identity {
    return new Identity($username, $email, $password, "");
  }

  /**
   * Make a new identity with a authentication hash.
   *
   * @return Identity
   */
  public static function make_with_auth_hash(string $auth_hash = ""): Identity {
    return new Identity("", "", "", $auth_hash);
  }

  /**
   * Get the identity's email.
   *
   * @return string
   */
  public function get_email(): string {
    return $this->email;
  }

  /**
   * Get the identity's username.
   *
   * @return string
   */
  public function get_username(): string {
    return $this->username;
  }

  /**
   * Get the identity's authentication hash, which can replace a username and password.
   *
   * @return string
   */
  public function get_authentication_hash(): string {
    return $this->authentication_hash;
  }

  /**
   * Get the identity's password.
   *
   * @return string
   */
  public function get_password(): string {
    return $this->password;
  }

  /**
   * Get valid identities what can be attempted to be used in authentication with the database.
   *
   * @return array Array of key-value pairs. Possible keys are "email", "username", and "authentication_hash".
   */
  public function get_valid_identities(): array {
    return $this->valid_identities;
  }
}

/**
 * User extends the Identity class and provides some extra features such as metadata tracking.
 */
class User extends Identity {
  private array $meta = [];

  /**
   * Get the first name of the user. If a first name isn't set, then an empty string is returned.
   *
   * @return string
   */
  public function get_first_name(): string {
    return isset($this->meta["first_name"]) ? $this->meta["first_name"] : "";
  }

  /**
   * Get the last name of the user. If a first name isn't set, then an empty string is returned.
   *
   * @return string
   */
  public function get_last_name(): string {
    return isset($this->meta["last_name"]) ? $this->meta["last_name"] : "";
  }

  /**
   * Set the user's first name.
   *
   * @param  string $value
   * @return void
   */
  public function set_first_name(string $value): void {
    if (is_string($value)) {
      $this->meta["first_name"] = $value;
    }
  }

  /**
   * Set the user's last name.
   *
   * @param  string $value
   * @return void
   */
  public function set_last_name(string $value): void {
    if (is_string($value)) {
      $this->meta["last_name"] = $value;
    }
  }

  public static function make_with_password(
    string $username = "",
    string $email = "",
    string $password = ""
  ): User {
    return new User($username, $email, $password, "");
  }

  public static function make_with_auth_hash(string $auth_hash = ""): User {
    return new User("", "", "", $auth_hash);
  }

  /**
   * Return this user's metadata array.
   *
   * @return array Array of metadata, indexed by meta key.
   */
  public function get_metadata(): array {
    return $this->metadata;
  }

  /**
   * Merge the user's metadata with some other metadata array. Should there be a collision between
   * metadata keys, the user's preexisting metadata key-value pairs are favored.
   *
   * @param  array $data Array of metadata.
   * @return void
   */
  public function set_metadata(array $data): void {
    $this->meta = array_merge($data, $this->meta);
  }
}
