<?php

require_once __DIR__ . "/Config.php";

/**
 * Request can decode and deserialize a JSON request as needed.
 */
class Request {
  private string $payload;
  private string $decoded;
  protected array $deserialized;

  private bool $was_encoded;

  /**
   * Given some payload, attempt to deserialize it. Should that fail, then try to decode and deserialized. If this fails, then the error bubbles outside of the class.
   *
   * @param  mixed $payload
   * @return void
   */
  public function __construct(string $payload) {
    $this->payload = $payload;
    try {
      // First try to directly deserialize the payload.
      $this->deserialized = $this->deserialize($payload);
      $this->was_encoded = false;
    } catch (Exception $e) {
      // Otherwise try to base 64 decode and then deserialize.
      $this->decoded = $this->decode($this->payload);
      $this->deserialized = $this->deserialize($this->decoded);
      $this->was_encoded = true;
    }
  }

  /**
   * Get some value from the request. This can be null.
   *
   * @param  string $key
   * @return mixed
   */
  public function get(string $key) {
    return $this->deserialized[$key];
  }

  /**
   * Get some value from the request as a string. Should they key not be in the request, then return
   * an empty string.
   *
   * @param  string $key
   * @return string
   */
  public function get_string(string $key): string {
    $value = $this->deserialized[$key];
    return isset($value) ? strval($value) : "";
  }

  /**
   * Get all possible keys for use with get and get_key.
   *
   * @return array
   */
  public function get_keys(): array {
    return array_keys($this->deserialized);
  }

  /**
   * Determine if the response was encoded or not. Useful for determining if some response to this
   * request should also be encoded.
   *
   * @return bool
   */
  public function was_encoded(): bool {
    return $this->was_encoded;
  }

  /**
   * Decode some base 64-encoded payload.
   *
   * @param  string $value
   * @return string
   */
  private static function decode(string $value): string {
    if (!is_string($value)) {
      throw new Exception("Payload is not a string.");
    }
    $decoded = base64_decode($value, $strict = true);
    if ($decoded === false) {
      throw new Exception("Payload could not be decoded.");
    } else {
      return $decoded;
    }
  }

  /**
   * Deserialize some string into an associative array.
   *
   * @param  string $value
   * @return array
   */
  private static function deserialize(string $value): array {
    if (!is_string($value)) {
      throw new Exception("Payload is not a string.");
    }
    $deserialized = json_decode($value, $flags = JSON_OBJECT_AS_ARRAY);
    if (is_null($deserialized)) {
      throw new Exception("Payload could not be deserialized.");
    } else {
      return $deserialized;
    }
  }
}

/**
 * A simple implementation of request that by default reads data in from php://input in the event
 * that $_POST is defined.
 */
final class PostRequest extends Request {
  public function __construct(string $source = "php://input") {
    if (isset($_POST)) {
      parent::__construct(file_get_contents($source));
    } else {
      throw new Exception("Request is not a POST request");
    }
  }
}

/**
 * A simple implementation of a GET request handler. Essentially just wraps $_GET.
 */
final class GetRequest extends Request {
  public function __construct() {
    if (isset($_GET)) {
      parent::__construct(json_encode($_GET));
    } else {
      throw new Exception("Request is not a GET request");
    }
  }
}
