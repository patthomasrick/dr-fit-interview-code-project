<?php

require_once __DIR__ . "/Config.php";
require_once __DIR__ . "/Mail.php";
require_once __DIR__ . "/Secret.php";
require_once __DIR__ . "/User.php";
require_once __DIR__ . "/Utilities.php";

const MOCK_STORE_LOCATION = __DIR__ . "/../auth_hash.txt";

/**
 * get_database gets a valid database handler. If possible, a MySQL database connection is returned.
 * Otherwise, for environments that do not have access to a MySQL server, a simple mock database is
 * returned.
 *
 * @return Database
 */
function get_database(): Database {
  $db = new MySQLDatabase(DB_DSN, DB_USER, DB_PASSWORD);
  if ($db->get_error()) {
    return new MockDatabase();
  } else {
    return $db;
  }
}

/**
 * Database defines the general interface in which the rest of the program will access the DB.
 */
abstract class Database {
  public function authenticate(Identity &$identity) {
    $valid_identities = $identity->get_valid_identities();
    if ($valid_identities["username"] || $valid_identities["email"]) {
      return $this->authenticate_password($identity);
    } elseif ($valid_identities["authentication_hash"]) {
      return $this->authenticate_auth_hash($identity);
    } else {
      return false;
    }
  }

  abstract protected function authenticate_password(Identity &$identity);

  abstract protected function authenticate_auth_hash(Identity &$identity): bool;

  /**
   * Create a new user in the database.
   *
   * @param  User $user User object.
   * @param  string $password User password to use on creation.
   * @return bool True on success, false on fail. Failure can be due to invalid parameters or that the account already exists.
   */
  abstract public function create_user(User &$user): bool;

  /**
   * Log the current user out of all accounts.
   *
   * @param  User $user
   * @return bool True if successful, false otherwise.
   */
  abstract public function log_out(Identity &$identity): bool;

  /**
   * Delete the user from the database.
   *
   * @param  User $user
   * @return bool True if successful, false otherwise.
   */
  abstract public function delete_user(Identity &$identity): bool;

  /**
   * Reset user, changing the account status from inactive to active.
   *
   * @param  string $unlock_code
   * @return bool True if successful, false otherwise.
   */
  abstract public function unlock_user(string $unlock_code): bool;
}

/**
 * MySQLDatabase
 */
final class MySQLDatabase extends Database {
  private $dbh;
  private string $error = "";

  public function __construct(string $db_connection_str, string $db_user, string $db_password) {
    try {
      $this->dbh = new PDO($db_connection_str, $db_user, $db_password);
    } catch (PDOException $e) {
      $this->error = "Database error: " . $e->getMessage();
    }
  }

  /**
   * Get the error, if any, after attempting to connect to the database. This function's primary use
   * is to check whether or not a valid MySQL connection can be established.
   *
   * @return string Error message as a string.
   */
  public function get_error(): string {
    return $this->error;
  }

  protected function authenticate_password(Identity &$identity): ?string {
    // First try to retrieve the user's data.
    $user_info = $this->lookup_user($identity->get_username(), $identity->get_email());
    if (!$user_info) {
      // Failed to lookup user and no data was returned.
      return "";
    }

    // Authenticate the user's password.
    $password_hash = $user_info["password"];
    $success = password_verify($identity->get_password(), $password_hash);
    $auth_hash = $success ? generate_authentication_hash() : "";

    // Insert information into the login table.
    $sql = "INSERT INTO `logins` (`successful`, `user_id`, `authentication_hash`, `ip_address`, `expires`)
    VALUES(:successful, :user_id, :authentication_hash, :ip_address, :expires);";
    $params = [
      ":successful" => intval($success),
      ":user_id" => $user_info["id"],
      ":authentication_hash" => simple_hash($auth_hash),
      ":ip_address" => get_remote_ip(),
      ":expires" => null,
    ];

    $statement = $this->dbh->prepare($sql);
    $status = $statement->execute($params);

    // If the last five attempts were unsuccessful, then lock out the user.
    if (!$success && isset($user_info["id"])) {
      $sql = "SELECT COUNT(`successful`) AS `num_attempts`, SUM(`successful`) AS `num_success`,
        ( SELECT COUNT(*) FROM `unlock` AS `t2` WHERE `t2`.`user_id` = :user_id AND `t2`.`expiration_date_time` > NOW()) AS `already_notified`
      FROM ( SELECT `successful` FROM `logins` WHERE `user_id` = :user_id ORDER BY `login_date_time` DESC LIMIT 5) as `t1`;";
      $params = [":user_id" => $user_info["id"]];
      $statement = $this->dbh->prepare($sql);
      $success = $statement->execute($params);
      $result = $statement->fetch(PDO::FETCH_ASSOC);

      $num_attempts = isset($result["num_attempts"]) ? intval($result["num_attempts"]) : 1;
      $num_successes = isset($result["num_success"]) ? intval($result["num_success"]) : 1;
      $already_notified = isset($result["already_notified"])
        ? intval($result["already_notified"])
        : 0;

      if ($success && $num_attempts >= 5 && $num_successes == 0 && $already_notified == 0) {
        // At least five attempts and no successes, time to lock out.
        $sql = "UPDATE `users` SET `users`.`active_user` = 0 WHERE `users`.`id` = :user_id;";
        $params = [":user_id" => $user_info["id"]];
        $statement = $this->dbh->prepare($sql);
        $statement->execute($params);

        $unlock_code = generate_authentication_hash();
        $sql = "INSERT INTO `unlock` (`user_id`, `unlock_code`) VALUES (:user_id, :unlock_code);";
        $params = [
          ":user_id" => $user_info["id"],
          ":unlock_code" => simple_hash($unlock_code),
        ];
        $statement = $this->dbh->prepare($sql);
        $statement->execute($params);

        send_email(
          $user_info["email"],
          "SSO API - Password reset",
          "Due to several failed login attempts, you account has been temporarily deactivated. Reactive your account by following this link: https://api.patrickwthomas.net/api/user/unlock.php?unlock_code=$unlock_code"
        );
      }
    }

    return $status ? $auth_hash : "";
  }

  protected function authenticate_auth_hash(Identity &$identity): bool {
    // Query selects the login attempt associated with the authentication hash is both successful
    // and the most recent attempt. The auth key also has to not be expired.
    $sql = "SELECT
      `logins`.`id`
    FROM
      `logins`
    INNER JOIN `users` ON `logins`.`user_id` = `users`.`id`
    WHERE
      `authentication_hash` = :authentication_hash AND 
      `expires` > NOW() AND (
      SELECT
        COUNT(`id`)
      FROM
        `logins` AS `t2`
      WHERE
        `t2`.`expires` > `logins`.`expires` AND `t2`.`successful` = 1 AND `logins`.`user_id` = `t2`.`user_id`
    ) = 0;";
    $params = [":authentication_hash" => simple_hash($identity->get_authentication_hash())];
    $statement = $this->dbh->prepare($sql);
    $status = $statement->execute($params);

    if (!$status) {
      return false;
    }

    $result = $statement->fetch(PDO::FETCH_ASSOC);
    if (!$result) {
      return false;
    }

    return true;
  }

  public function create_user(User &$user): bool {
    $valid_identities = $user->get_valid_identities();
    if (
      !(
        $valid_identities["username"] ||
        $valid_identities["email"] ||
        $valid_identities["password"]
      )
    ) {
      // Require that the user has a valid username, email, and password.
      return false;
    } elseif (
      $this->identity_taken($user->get_username()) ||
      $this->identity_taken($user->get_email())
    ) {
      // Do not create the account of the user already exists.
      return false;
    }

    // Password needs to be hashed before insertion into the database.
    $password_hash = password_hash($user->get_password(), PASSWORD_DEFAULT);

    // Insert information into the user table.
    $sql = "INSERT INTO `users`
      (`username`, `password`, `email`, `first_name`, `last_name`)
    VALUES
      (:username, :password, :email, :first_name, :last_name);";
    $params = [
      "username" => $user->get_username(),
      "password" => $password_hash,
      "email" => $user->get_email(),
      "first_name" => $user->get_first_name(),
      "last_name" => $user->get_last_name(),
    ];

    $statement = $this->dbh->prepare($sql);
    $status = $statement->execute($params);

    return $status;
  }

  public function log_out(Identity &$identity): bool {
    return false;
  }

  public function delete_user(Identity &$identity): bool {
    if ($this->authenticate($identity)) {
      $sql = "";
      $params = [];
      if ($identity->get_email()) {
        $sql = "DELETE FROM `users` WHERE `email` = :email;";
        $params = [":email" => $identity->get_email()];
      } elseif ($identity->get_username()) {
        $sql = "DELETE FROM `users` WHERE `username` = :username;";
        $params = [":username" => $identity->get_username()];
      } else {
        return false;
      }

      $statement = $this->dbh->prepare($sql);
      $status = $statement->execute($params);
      return boolval($status);
    } else {
      return false;
    }
  }

  private function lookup_user(string $username, string $email): array {
    // Build the query based on the available identities.
    $sql = "SELECT * FROM `users` WHERE `active_user` = 1 AND ";
    $params = [];
    if ($email && $username) {
      $sql .= "`email` = :email AND `username` = :username;";
      $params = [":email" => $email, ":username" => $username];
    } elseif ($email) {
      $sql .= "`email` = :email;";
      $params = [":email" => $email];
    } elseif ($username) {
      $sql .= "`username` = :username;";
      $params = [":username" => $username];
    } else {
      // No valid identity to use from the user; fail.
      return [];
    }

    // Execute the SQL query to retrieve the stored password hash.
    $statement = $this->dbh->prepare($sql);
    $statement->execute($params);
    $result = $statement->fetch(PDO::FETCH_ASSOC);

    if (!$result) {
      return [];
    } else {
      return $result;
    }
  }

  private function identity_taken(string $identity): bool {
    $sql =
      "SELECT COUNT(`id`) AS `num_accounts` FROM `users` WHERE `users`.`username` = :identity OR `users`.`email` = :identity;";
    $params = [":identity" => $identity];

    // Execute the SQL query to retrieve the stored password hash.
    $statement = $this->dbh->prepare($sql);
    $statement->execute($params);
    $result = $statement->fetch(PDO::FETCH_ASSOC);
    $num_accounts = isset($result["num_accounts"]) ? $result["num_accounts"] : 1;

    // False means failure, result >= 1 means account is taken.
    return $num_accounts !== "0";
  }

  public function unlock_user(string $unlock_code): bool {
    $hashed_code = simple_hash($unlock_code); // Insecure but suffices for a demo.
    $sql =
      "SELECT `id` FROM `unlock` WHERE `unlock_code` = :unlock_code AND `expiration_date_time` > NOW();";
    $params = [":unlock_code" => $hashed_code];

    // Execute the SQL query to validate the unlock code.
    $statement = $this->dbh->prepare($sql);
    $success = $statement->execute($params);
    if (!$success) {
      return false;
    }
    $result = $statement->fetch(PDO::FETCH_ASSOC);
    $unlock_id = isset($result["id"]) ? intval($result["id"]) : -1;

    // If a code exists, then this is valid.
    if ($unlock_id === -1) {
      return false;
    }

    // Since this is valid, remove the old request and set the user's account back to active.
    $sql =
      "UPDATE `users` SET `users`.`active_user` = 1 WHERE `users`.`id` = (SELECT `unlock`.`user_id` FROM `unlock` WHERE `unlock`.`id` = :id LIMIT 1);";
    $params = [":id" => $unlock_id];
    $statement = $this->dbh->prepare($sql);
    $success = $statement->execute($params);
    if (!$success) {
      return false;
    }

    // Now remove the request.
    $sql = "DELETE FROM `unlock` WHERE `unlock`.`id` = :id;";
    $params = [":id" => $unlock_id];
    $statement = $this->dbh->prepare($sql);
    $success = $statement->execute($params);
    if (!$success) {
      return false;
    }

    return true;
  }
}

final class MockDatabase extends Database {
  private $users = [];

  public function __construct() {
    $user = User::make_with_password("test_user", "test_user@testuser.com", "password");
    $user->set_first_name("Test");
    $user->set_last_name("User");
    $this->users[] = $user;
  }

  protected function authenticate_password(Identity &$identity) {
    foreach ($this->users as $user) {
      if ($identity->get_password() === $user->get_password()) {
        return generate_authentication_hash();
      }
    }
    return false;
  }

  protected function authenticate_auth_hash(Identity &$identity): bool {
    foreach ($this->users as $user) {
      if ($identity->get_authentication_hash() === $user->get_authentication_hash()) {
        return true;
      }
    }
    return false;
  }

  public function create_user(User &$user): bool {
    $this->users[] = $user;
    return true;
  }

  public function log_out(Identity &$identity): bool {
    return false;
  }

  public function delete_user(Identity &$identity): bool {
    return false;
  }

  public function unlock_user(string $unlock_code): bool {
    return false;
  }
}
