<?php

require_once __DIR__ . "/Config.php";

function get_remote_ip(): string {
  // From https://www.w3docs.com/snippets/php/get-remote-ip-address-in-php.html.
  $ip = "0.0.0.0";
  if (!empty($_SERVER["HTTP_CLIENT_IP"])) {
    $ip = $_SERVER["HTTP_CLIENT_IP"];
  } elseif (!empty($_SERVER["HTTP_X_FORWARDED_FOR"])) {
    $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
  } elseif (!empty($_SERVER["REMOTE_ADDR"])) {
    $ip = $_SERVER["REMOTE_ADDR"];
  }
  return $ip;
}

function generate_authentication_hash(): string {
  // Generate 64 random hex digits as the authentication key.
  $authentication_hash = bin2hex(random_bytes(32));
  return $authentication_hash;
}

function send_post_request(string $url, array $data) {
  $payload = json_encode($data);
  $curl = curl_init($url);
  curl_setopt($curl, CURLOPT_POST, true);
  curl_setopt($curl, CURLOPT_HTTPHEADER, ["Content-Type:application/json"]);
  curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  $response = curl_exec($curl);
  if ($response === false) {
    throw new Exception("No connection.");
  }
  return json_decode($response, $flags = JSON_OBJECT_AS_ARRAY);
}

function send_encoded_post_request(string $url, array $data) {
  $payload = base64_encode(json_encode($data));
  $curl = curl_init($url);
  curl_setopt($curl, CURLOPT_POST, true);
  curl_setopt($curl, CURLOPT_HTTPHEADER, ["Content-Type:application/json"]);
  curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  $response = curl_exec($curl);
  if ($response === false) {
    throw new Exception("No connection.");
  }
  return json_decode($response, $flags = JSON_OBJECT_AS_ARRAY);
}

function get_env_or_default(string $env_key_name, string $default): string {
  if (empty($env_key_name)) {
    return "";
  }
  $value = getenv($env_key_name);
  if ($value === false) {
    return strval($default);
  } else {
    return strval($value);
  }
}

function simple_hash($val) {
  return hash(HASH_ALGO, $val);
}
