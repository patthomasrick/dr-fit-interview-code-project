<?php

require_once __DIR__ . "/OnlineTestConfig.php";
require_once __DIR__ . "/../include/Secret.php";
require_once __DIR__ . "/../include/Utilities.php";

use PHPUnit\Framework\TestCase;

final class ApiLoginTest extends TestCase {
  private $TEST_USER_1 = [
    "username" => "test_user1",
    "email" => "test_user1@patrickwthomas.net",
    "password" => "test_user1",
    "first_name" => "Test1",
    "last_name" => "User1",
  ];

  public function setUp(): void {
    try {
      send_post_request(DEV_API_URL . "/api/user/create.php", $this->TEST_USER_1);
      // send_post_request(DEV_API_URL . "/api/user/create.php", $this->TEST_USER_2);
    } catch (Exception $e) {
      $this->markTestSkipped($e);
    }
  }

  public function tearDown(): void {
    send_post_request(DEV_API_URL . "/api/user/delete.php", $this->TEST_USER_1);
    // send_post_request(DEV_API_URL . "/api/user/delete.php", $this->TEST_USER_2);
  }

  public function testLoginEmailAndUsername(): void {
    $response = send_post_request(DEV_API_URL . "/api/user/login.php", $this->TEST_USER_1);
    $this->assertEquals("success", $response["status"]);
  }

  public function testLoginEmailOnly(): void {
    $u = $this->TEST_USER_1;
    unset($u["email"]);
    $response = send_post_request(DEV_API_URL . "/api/user/login.php", $u);
    $this->assertEquals("success", $response["status"]);
  }

  public function testLoginUsernameOnly(): void {
    $u = $this->TEST_USER_1;
    unset($u["username"]);
    $response = send_post_request(DEV_API_URL . "/api/user/login.php", $u);
    $this->assertEquals("success", $response["status"]);
  }

  public function testLoginWrongPassword(): void {
    $u = $this->TEST_USER_1;
    $u["password"] = "wrong";
    $response = send_post_request(DEV_API_URL . "/api/user/login.php", $u);
    $this->assertNotEquals("success", $response["status"]);
  }

  public function testLoginNoUsernameOrEmail(): void {
    $u = $this->TEST_USER_1;
    unset($u["email"]);
    unset($u["username"]);
    $response = send_post_request(DEV_API_URL . "/api/user/login.php", $u);
    $this->assertNotEquals("success", $response["status"]);
  }

  public function testLoginNoPassword(): void {
    $u = $this->TEST_USER_1;
    unset($u["password"]);
    $response = send_post_request(DEV_API_URL . "/api/user/login.php", $u);
    $this->assertNotEquals("success", $response["status"]);
  }

  public function testLoginNoCredentials(): void {
    $u = [];
    $response = send_post_request(DEV_API_URL . "/api/user/login.php", $u);
    $this->assertNotEquals("success", $response["status"]);
  }
}
