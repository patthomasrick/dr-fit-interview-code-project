<?php

require_once __DIR__ . "/OnlineTestConfig.php";
require_once __DIR__ . "/../include/Secret.php";
require_once __DIR__ . "/../include/Utilities.php";

use PHPUnit\Framework\TestCase;

final class ApiAuthenticateTest extends TestCase {
  private $TEST_USER_1 = [
    "username" => "test_user1",
    "email" => "test_user1@patrickwthomas.net",
    "password" => "test_user1",
    "first_name" => "Test1",
    "last_name" => "User1",
  ];

  private string $auth_hash_1;

  public function setUp(): void {
    try {
      send_post_request(DEV_API_URL . "/api/user/delete.php", $this->TEST_USER_1);
      send_post_request(DEV_API_URL . "/api/user/create.php", $this->TEST_USER_1);
      $r = send_post_request(DEV_API_URL . "/api/user/login.php", $this->TEST_USER_1);
      $this->auth_hash_1 = $r["authentication_hash"];
    } catch (Exception $e) {
      $this->markTestSkipped($e);
    }
  }

  public function tearDown(): void {
    send_post_request(DEV_API_URL . "/api/user/delete.php", $this->TEST_USER_1);
  }

  public function testAuthenticate(): void {
    // Ideal use case.
    $response = send_post_request(DEV_API_URL . "/api/user/authenticate.php", [
      "authentication_hash" => $this->auth_hash_1,
    ]);
    $this->assertEquals("success", $response["status"]);
  }

  public function testAuthenticateInvalid(): void {
    // Send an auth key that doesn't exist.
    $response = send_post_request(DEV_API_URL . "/api/user/authenticate.php", [
      "authentication_hash" => $this->auth_hash_1 . "a",
    ]);
    $this->assertEquals("failed", $response["status"]);
  }

  // public function testAuthenticateInvalidOldKey(): void {
  //   // Log in again and invalidate the old key.
  //   $new_key = send_post_request(DEV_API_URL . "/api/user/login.php", $this->TEST_USER_1)["authentication_hash"];
  //   $response = send_post_request(DEV_API_URL . "/api/user/authenticate.php", [
  //     "authentication_hash" => $new_key,
  //   ]);
  //   $this->assertEquals("success", $response["status"]);
  //   $response = send_post_request(DEV_API_URL . "/api/user/authenticate.php", [
  //     "authentication_hash" => $this->auth_hash_1,
  //   ]);
  //   $this->assertEquals("failed", $response["status"]);
  // }

  public function testEncodedAuthenticate(): void {
    // Ideal use case.
    $response = send_encoded_post_request(DEV_API_URL . "/api/user/authenticate.php", [
      "authentication_hash" => $this->auth_hash_1,
    ]);
    $this->assertEquals("success", $response["status"]);
  }

  public function testEncodedAuthenticateInvalid(): void {
    // Send an auth key that doesn't exist.
    $response = send_encoded_post_request(DEV_API_URL . "/api/user/authenticate.php", [
      "authentication_hash" => $this->auth_hash_1 . "a",
    ]);
    $this->assertEquals("failed", $response["status"]);
  }

  // public function testEncodedAuthenticateInvalidOldKey(): void {
  //   // Log in again and invalidate the old key.
  //   $new_key = send_encoded_post_request(DEV_API_URL . "/api/user/login.php", $this->TEST_USER_1)["authentication_hash"];
  //   $response = send_encoded_post_request(DEV_API_URL . "/api/user/authenticate.php", [
  //     "authentication_hash" => $new_key,
  //   ]);
  //   $this->assertEquals("success", $response["status"]);
  //   $response = send_encoded_post_request(DEV_API_URL . "/api/user/authenticate.php", [
  //     "authentication_hash" => $this->auth_hash_1,
  //   ]);
  //   $this->assertEquals("failed", $response["status"]);
  // }
}
