<?php

require_once __DIR__ . "/OnlineTestConfig.php";
require_once __DIR__ . "/../include/Secret.php";
require_once __DIR__ . "/../include/Utilities.php";

use PHPUnit\Framework\TestCase;

final class ApiCreateTest extends TestCase {
  public function setUp(): void {
    try {
      send_post_request(DEV_API_URL . "/api/user/delete.php", [
        "username" => "test_user",
        "email" => "test_user@patrickwthomas.net",
        "password" => "test_user",
      ]);
    } catch (Exception $e) {
      $this->markTestSkipped($e);
    }
  }

  public function tearDown(): void {
    send_post_request(DEV_API_URL . "/api/user/delete.php", [
      "username" => "test_user",
      "email" => "test_user@patrickwthomas.net",
      "password" => "test_user",
    ]);
  }

  public function testCreate() {
    $response = send_post_request(DEV_API_URL . "/api/user/create.php", [
      "username" => "test_user",
      "email" => "test_user@patrickwthomas.net",
      "password" => "test_user",
      "first_name" => "Test",
      "last_name" => "User",
    ]);

    $this->assertEquals("success", $response["status"]);
  }

  /**
   * @dataProvider collisionUserProvider
   */
  public function testCreateCollision($username, $email) {
    $response = send_post_request(DEV_API_URL . "/api/user/create.php", [
      "username" => "test_user",
      "email" => "test_user@patrickwthomas.net",
      "password" => "test_user",
      "first_name" => "Test",
      "last_name" => "User",
    ]);

    $this->assertEquals("success", $response["status"]);

    $response = send_post_request(DEV_API_URL . "/api/user/create.php", [
      "username" => $username,
      "email" => $email,
      "password" => "test_user",
      "first_name" => "Test",
      "last_name" => "User",
    ]);

    $this->assertEquals("failed", $response["status"]);
  }

  public function collisionUserProvider(): array {
    return [
      ["test_user", "test_user@patrickwthomas.net"],
      ["other_test_user", "test_user@patrickwthomas.net"],
      ["test_user", "other_test_user@patrickwthomas.net"],
    ];
  }
}
