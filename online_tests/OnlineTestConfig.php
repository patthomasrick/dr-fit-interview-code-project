<?php

require_once __DIR__ . "/../include/Config.php";
require_once __DIR__ . "/../include/Utilities.php";

define("DEV_API_URL", get_env_or_default("DEV_API_URL", "http://localhost:8080"));
