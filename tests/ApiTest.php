<?php

require_once __DIR__ . "/../include/Api.php";

use PHPUnit\Framework\TestCase;

final class TestEndpoint extends Endpoint {
  protected function _run(): bool {
    $this->update_response_value("test key", "test value");
    return true;
  }
}

final class ApiTest extends TestCase {
  public function testSimpleEndpoint() {
    $endpoint = new SimpleEndpoint(["some_value" => "true"]);
    $expected = json_encode(
      [
        "some_value" => "true",
        "timestamp" => (new DateTime())->format("Y-m-d H:i:s"),
        "status" => "success",
      ],
      JSON_PRETTY_PRINT
    );
    $this->expectOutputString($expected);
    $endpoint->run();
  }

  public function testSimpleEndpointEncoded() {
    $endpoint = new SimpleEndpoint(["some_value" => "true"], true);
    $expected = base64_encode(
      json_encode(
        [
          "some_value" => "true",
          "timestamp" => (new DateTime())->format("Y-m-d H:i:s"),
          "status" => "success",
        ],
        JSON_PRETTY_PRINT
      )
    );
    $this->expectOutputString($expected);
    $endpoint->run();
  }

  public function testErrorEndpoint() {
    $endpoint = new ErrorEndpoint(new Exception("Test error message."));
    $expected = json_encode(
      [
        "error" => "Test error message.",
        "timestamp" => (new DateTime())->format("Y-m-d H:i:s"),
        "status" => "error",
      ],
      JSON_PRETTY_PRINT
    );
    $this->expectOutputString($expected);
    $endpoint->run();
  }

  public function testErrorEndpointEncoded() {
    $endpoint = new ErrorEndpoint(new Exception("Test error message."), true);
    $expected = base64_encode(
      json_encode(
        [
          "error" => "Test error message.",
          "timestamp" => (new DateTime())->format("Y-m-d H:i:s"),
          "status" => "error",
        ],
        JSON_PRETTY_PRINT
      )
    );
    $this->expectOutputString($expected);
    $endpoint->run();
  }

  public function testEndpointUpdateResponseValue() {
    $endpoint = new TestEndpoint();
    $expected = json_encode(
      [
        "test key" => "test value",
        "timestamp" => (new DateTime())->format("Y-m-d H:i:s"),
        "status" => "success",
      ],
      JSON_PRETTY_PRINT
    );
    $this->expectOutputString($expected);
    $endpoint->run();
  }

  public function testEndpointEncodedUpdateResponseValue() {
    $endpoint = new TestEndpoint(true);
    $expected = base64_encode(
      json_encode(
        [
          "test key" => "test value",
          "timestamp" => (new DateTime())->format("Y-m-d H:i:s"),
          "status" => "success",
        ],
        JSON_PRETTY_PRINT
      )
    );
    $this->expectOutputString($expected);
    $endpoint->run();
  }
}
