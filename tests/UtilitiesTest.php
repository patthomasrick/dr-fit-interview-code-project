<?php

require_once __DIR__ . "/../include/Utilities.php";

use PHPUnit\Framework\TestCase;

final class UtilitiesTest extends TestCase {
  /**
   * @dataProvider keyProvider
   */
  public function testRandomAuthKeys($key): void {
    $this->assertEquals(64, strlen($key));
  }

  public function keyProvider(): array {
    $output = [];
    for ($i = 0; $i < 10; $i++) {
      $output[] = [generate_authentication_hash()];
    }
    return $output;
  }
}
