<?php

require_once __DIR__ . "/../include/Requests.php";

use PHPUnit\Framework\TestCase;

final class RequestsTest extends TestCase {
  public function testRequestValid(): void {
    $payload = json_encode(["username" => "user", "password" => "password"]);
    $request = new Request($payload);
    $this->assertEquals("user", $request->get("username"));
    $this->assertEquals("password", $request->get("password"));
  }

  public function testRequestEncodedValid(): void {
    $payload = base64_encode(json_encode(["username" => "user", "password" => "password"]));
    $request = new Request($payload);
    $this->assertEquals("user", $request->get("username"));
    $this->assertEquals("password", $request->get("password"));
  }

  public function testRequestInvalid1(): void {
    $this->expectException("Exception");
    // Valid payload with extra character?
    $payload = base64_encode(json_encode(["username" => "user", "password" => "password"])) . "a";
    new Request($payload);
  }

  public function testRequestInvalid2(): void {
    $this->expectException("Exception");
    // No payload given.
    $payload = "";
    new Request($payload);
  }
}
