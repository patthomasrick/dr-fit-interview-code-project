<?php

require_once __DIR__ . "/../include/Config.php";
require_once __DIR__ . "/../include/User.php";
require_once __DIR__ . "/../include/Utilities.php";

use PHPUnit\Framework\TestCase;

final class UserTest extends TestCase {
  public function testIdentityConstructor(): void {
    $identity = Identity::make_with_password("test_user", "test@test.com", "password");
    $this->assertInstanceOf(Identity::class, $identity);
  }

  public function testIdentityUsernameOnly(): void {
    $identity = Identity::make_with_password("test_user", "", "password");
    $this->assertInstanceOf(Identity::class, $identity);
  }

  public function testIdentityEmailOnly(): void {
    $identity = Identity::make_with_password("", "test@test.com", "password");
    $this->assertInstanceOf(Identity::class, $identity);
  }

  public function testIdentityAuthKeyOnly(): void {
    $identity = Identity::make_with_auth_hash(generate_authentication_hash());
    $this->assertInstanceOf(Identity::class, $identity);
  }

  public function testUserConstructor(): void {
    $u = User::make_with_password("test_user", "test@test.com", "password");
    $this->assertInstanceOf(User::class, $u);
  }

  public function testUserUsernameOnly(): void {
    $u = User::make_with_password("test_user", "", "password");
    $this->assertInstanceOf(User::class, $u);
  }

  public function testUserEmailOnly(): void {
    $u = User::make_with_password("", "test@test.com", "password");
    $this->assertInstanceOf(User::class, $u);
  }

  public function testUserAuthKeyOnly(): void {
    $u = User::make_with_auth_hash(generate_authentication_hash());
    $this->assertInstanceOf(User::class, $u);
  }

  public function testIdentityUsernameGetter(): void {
    $identity = Identity::make_with_password("test_user", "test@test.com", "password");
    $this->assertEquals("test_user", $identity->get_username());
  }

  public function testIdentityEmailGetter(): void {
    $identity = Identity::make_with_password("test_user", "test@test.com", "password");
    $this->assertEquals("test@test.com", $identity->get_email());
  }

  /**
   * @dataProvider invalidUsernameProvider
   */
  public function testIdentityInvalidUsername($username): void {
    $this->expectException("Exception");
    Identity::make_with_password($username, "", "password");
  }

  /**
   * @dataProvider invalidEmailProvider
   */
  public function testIdentityInvalidEmail($email): void {
    $this->expectException("Exception");
    Identity::make_with_password("", $email, "password");
  }

  /**
   * @dataProvider invalidPasswordProvider
   */
  public function testIdentityInvalidPassword($password): void {
    $this->expectException("Exception");
    Identity::make_with_password("test_user", "test@test.com", $password);
  }

  public function invalidUsernameProvider(): array {
    return [[""], ["a"], ["aa"]];
  }

  public function invalidEmailProvider(): array {
    return [[""], ["a"], ["aa"], ["a@a"], ["me@mail"], ["@gmail.com"], ["me@.com"], ["me@gmail.c"]];
  }

  public function invalidPasswordProvider(): array {
    $output = [[""]];
    for ($i = 0; $i < 8; $i++) {
      $output[] = [str_repeat("a", $i)];
    }
    return $output;
  }
}
