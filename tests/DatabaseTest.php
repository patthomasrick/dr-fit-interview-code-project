<?php

require_once __DIR__ . "/../include/Database.php";
require_once __DIR__ . "/../include/Secret.php";

use PHPUnit\Framework\TestCase;

final class DatabaseTest extends TestCase {
  public function testConstruct(): void {
    $this->assertInstanceOf(Database::class, new MockDatabase());
  }

  public function testGetDatabaseHandle(): void {
    $db = get_database();
    $this->assertInstanceOf(Database::class, $db);
  }

  public function testMockAuthenticateValidPassword(): void {
    $identity = Identity::make_with_password("test_user", "test_user@testuser.com", "password");
    $db = new MockDatabase();
    $this->assertNotFalse($db->authenticate($identity));
  }

  public function testMockAuthenticateInvalidPassword(): void {
    $identity = Identity::make_with_password("test_user", "test_user@testuser.com", "password1");
    $db = new MockDatabase();
    $this->assertFalse($db->authenticate($identity));
  }

  // Hard to test MySQL connection with unit tests; will limit its testing to system/integration testing for now.
}
