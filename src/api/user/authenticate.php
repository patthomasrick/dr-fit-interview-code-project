<?php

require_once __DIR__ . "/../../../include/Api.php";
require_once __DIR__ . "/../../../include/Config.php";
require_once __DIR__ . "/../../../include/Database.php";
require_once __DIR__ . "/../../../include/Requests.php";
require_once __DIR__ . "/../../../include/User.php";

final class AuthenticationEndpoint extends Endpoint {
  public function __construct(Request $request, Database $database, bool $encoded = false) {
    $this->request = $request;
    $this->database = $database;
    parent::__construct($encoded);
  }

  protected function _run(): bool {
    if (!isset($this->database)) {
      $this->error("Couldn't connect to database");
    }

    $auth_hash = $this->request->get_string("authentication_hash");
    $identity = Identity::make_with_auth_hash($auth_hash);
    $success = $this->database->authenticate($identity);

    return $success;
  }
}

try {
  $db = get_database();
  $request = new PostRequest();
  $endpoint = new AuthenticationEndpoint($request, $db);
} catch (Throwable $th) {
  $endpoint = new ErrorEndpoint($th);
}
$endpoint->run();
die();
