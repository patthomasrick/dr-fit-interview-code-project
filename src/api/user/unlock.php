<?php

require_once __DIR__ . "/../../../include/Api.php";
require_once __DIR__ . "/../../../include/Config.php";
require_once __DIR__ . "/../../../include/Database.php";
require_once __DIR__ . "/../../../include/Requests.php";
require_once __DIR__ . "/../../../include/User.php";

final class UnlockEndpoint extends Endpoint {
  public function __construct(Request $request, Database $database, bool $encoded = false) {
    $this->request = $request;
    $this->database = $database;
    parent::__construct($encoded);
  }

  protected function _run(): bool {
    if (!isset($this->database)) {
      $this->error("Couldn't connect to database");
    }

    $unlock_code = $this->request->get_string("unlock_code");
    $success = $this->database->unlock_user($unlock_code);

    return $success;
  }
}

try {
  $db = get_database();
  $request = new GetRequest();
  $endpoint = new UnlockEndpoint($request, $db);
} catch (Throwable $th) {
  $endpoint = new ErrorEndpoint($th);
}
$endpoint->run();
die();
