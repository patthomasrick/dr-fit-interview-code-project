<?php

require_once __DIR__ . "/../../../include/Api.php";
require_once __DIR__ . "/../../../include/Config.php";
require_once __DIR__ . "/../../../include/Database.php";
require_once __DIR__ . "/../../../include/Mail.php";
require_once __DIR__ . "/../../../include/Requests.php";
require_once __DIR__ . "/../../../include/User.php";

final class CreateEndpoint extends Endpoint {
  public function __construct(Request $request, Database $database, bool $encoded = false) {
    $this->request = $request;
    $this->database = $database;
    parent::__construct($encoded);
  }

  protected function _run(): bool {
    if (!isset($this->database)) {
      $this->error("Couldn't connect to database");
    }

    $username = $this->request->get_string("username");
    $email = $this->request->get_string("email");
    $password = $this->request->get_string("password");
    $first_name = $this->request->get_string("first_name");
    $last_name = $this->request->get_string("last_name");

    $user = null;
    if (!is_valid_email($email) || !is_valid_username($username)) {
      $this->error("A valid username and email address are required to create an account.");
    }
    $user = User::make_with_password($username, $email, $password);
    $user->set_first_name($first_name);
    $user->set_last_name($last_name);

    $success = $this->database->create_user($user, $password, $first_name, $last_name);

    if ($success && in_array("email", $user->get_valid_identities())) {
      send_email(
        $user->get_email(),
        "Welcome to SSO Test!",
        "Congratulations on making an account with my test SSO API!"
      );
    }

    return $success;
  }
}

try {
  $db = get_database();
  $request = new PostRequest();
  $endpoint = new CreateEndpoint($request, $db);
} catch (Throwable $th) {
  $endpoint = new ErrorEndpoint($th);
}
$endpoint->run();
die();
