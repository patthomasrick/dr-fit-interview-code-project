<?php

require_once __DIR__ . "/../../../include/Api.php";
require_once __DIR__ . "/../../../include/Config.php";
require_once __DIR__ . "/../../../include/Database.php";
require_once __DIR__ . "/../../../include/Requests.php";
require_once __DIR__ . "/../../../include/User.php";

final class LoginEndpoint extends Endpoint {
  public function __construct(Request $request, Database $database, bool $encoded = false) {
    $this->request = $request;
    $this->database = $database;
    parent::__construct($encoded);
  }

  protected function _run(): bool {
    if (!isset($this->database)) {
      $this->error("Couldn't connect to database");
    }

    $username = $this->request->get_string("username");
    $email = $this->request->get_string("email");
    $password = $this->request->get_string("password");

    if (($username || $email) && $password) {
      $identity = Identity::make_with_password($username, $email, $password);
      return $this->database->delete_user($identity);
    }

    return false;
  }
}

try {
  $db = get_database();
  $request = new PostRequest();
  $endpoint = new LoginEndpoint($request, $db);
} catch (Throwable $th) {
  $endpoint = new ErrorEndpoint($th);
}
$endpoint->run();
die();
