<?php

require_once __DIR__ . "/../../../include/Api.php";
require_once __DIR__ . "/../../../include/Config.php";
require_once __DIR__ . "/../../../include/Database.php";
require_once __DIR__ . "/../../../include/Requests.php";
require_once __DIR__ . "/../../../include/User.php";

final class LoginEndpoint extends Endpoint {
  public function __construct(Request $request, Database $database, bool $encoded = false) {
    $this->request = $request;
    $this->database = $database;
    parent::__construct($encoded);
  }

  protected function _run(): bool {
    if (!isset($this->database)) {
      $this->error("Couldn't connect to database");
    }

    $username = $this->request->get_string("username");
    $email = $this->request->get_string("email");
    $password = $this->request->get_string("password");

    $identity = Identity::make_with_password($username, $email, $password);
    $auth_hash = $this->database->authenticate($identity);

    if (empty($auth_hash) || $auth_hash === false) {
      $this->error("Failed to authenticate user; user does not exist or password is incorrect.");
    } else {
      // Success; return auth key.
      $this->update_response_value("authentication_hash", $auth_hash);
    }

    return true;
  }
}

try {
  $db = get_database();
  $request = new PostRequest();
  $endpoint = new LoginEndpoint($request, $db);
} catch (Throwable $th) {
  $endpoint = new ErrorEndpoint($th);
}
$endpoint->run();
die();
