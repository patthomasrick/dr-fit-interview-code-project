const API_URL = "https://api.patrickwthomas.net";

function ajax_log_response(request, submit_button_id) {
  let submit_button = document.getElementById(submit_button_id);
  submit_button.classList.add("is-loading");
  return () => {
    if (request.readyState === XMLHttpRequest.DONE) {
      console.log(request.responseText);
      append_to_output(request.responseText);
      submit_button.classList.remove("is-loading");
    } else {
      // Not ready yet.
    }
  };
}

function login_form_submit() {
  // Get the username and password from the form.
  let username = document.getElementById("form-username").value;
  let email = "";
  if (username.includes("@")) {
    email = username;
    username = "";
  }
  let password = document.getElementById("form-password").value;
  let payload = { username: username, email: email, password: password };

  let request = new XMLHttpRequest();
  request.open("POST", API_URL + "/api/user/login.php");
  request.setRequestHeader("Content-Type", "application/json");
  request.onreadystatechange = ajax_log_response(request, "login-form-submit");
  let payload_s = btoa(JSON.stringify(payload));
  // append_to_output(payload_s);
  request.send(payload_s);
}

function auth_form_submit() {
  // Get the username and password from the form.
  let auth_hash = document.getElementById("form-auth-hash").value;
  let payload = { authentication_hash: auth_hash };

  let request = new XMLHttpRequest();
  request.open("POST", API_URL + "/api/user/authenticate.php");
  request.setRequestHeader("Content-Type", "application/json");
  request.onreadystatechange = ajax_log_response(request, "validate-form-submit");
  let payload_s = btoa(JSON.stringify(payload));

  request.send(payload_s);
}

function create_form_submit() {
  // Get the username and password from the form.
  let username = document.getElementById("form-create-username").value;
  let email = document.getElementById("form-create-email").value;
  let first_name = document.getElementById("form-create-first-name").value;
  let last_name = document.getElementById("form-create-last-name").value;
  let password = document.getElementById("form-create-password").value;
  let payload = {
    username: username,
    email: email,
    password: password,
    first_name: first_name,
    last_name: last_name,
  };

  let request = new XMLHttpRequest();
  request.open("POST", API_URL + "/api/user/create.php");
  request.setRequestHeader("Content-Type", "application/json");
  request.onreadystatechange = ajax_log_response(request, "create-form-submit");
  let payload_s = btoa(JSON.stringify(payload));
  // append_to_output(payload_s);
  request.send(payload_s);
}

function delete_form_submit() {
  // Get the username and password from the form.
  let username = document.getElementById("form-delete-username").value;
  let email = document.getElementById("form-delete-email").value;
  let password = document.getElementById("form-delete-password").value;
  let payload = {
    username: username,
    email: email,
    password: password,
  };

  let request = new XMLHttpRequest();
  request.open("POST", API_URL + "/api/user/delete.php");
  request.setRequestHeader("Content-Type", "application/json");
  request.onreadystatechange = ajax_log_response(request, "delete-form-submit");
  let payload_s = btoa(JSON.stringify(payload));
  // append_to_output(payload_s);
  request.send(payload_s);
}

function append_to_output(s) {
  let output_pre = document.getElementById("output");
  let old_text = output_pre.textContent;
  let new_text = s + "\n\n" + old_text;
  output_pre.textContent = new_text;

  // Scroll to top after new output (#2).
  window.scrollTo(0, 0);
}
